from crispy_forms.helper import FormHelper
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=150, label='', widget=forms.TextInput(
        attrs={
            'placeholder': 'First name',
            'aria-label' : 'First name'
        }))
    last_name = forms.CharField(max_length=150, label='', widget=forms.TextInput(attrs={'placeholder': 'Last name'}))
    email = forms.EmailField(max_length=150, label='', widget=forms.EmailInput(attrs={'id': 'email', 'placeholder': 'Email'}))
    password1 = forms.CharField(label='', widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))
    password2 = forms.CharField(label='', widget=forms.PasswordInput(attrs={'placeholder': 'ConfirmPassword'}))
    username = forms.CharField(max_length=150, label='', widget=forms.TextInput(
        attrs={
            'id': 'username',
            'placeholder': 'User name',
            'style': 'display: none'
        }))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()

    class Meta:
        model = User
        # widgets = {
        #     'password1': forms.PasswordInput()
        # }
        fields = ('username', 'first_name', 'last_name',
                  'email', 'password1', 'password2')
