from django.conf.urls import url
from django.urls import include

from . import views

urlpatterns = [
    url(r'^login/$', views.user_login, name='login'),
    url(r'^sing_up/$', views.sing_up, name='singup'),
    url(r'^profile/$', views.profile, name='profile'),
    url('', include('social_django.urls', namespace='social')),
]
